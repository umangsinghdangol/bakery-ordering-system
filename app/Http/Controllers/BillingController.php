<?php

namespace App\Http\Controllers;

use App\Models\Order;
use Illuminate\Http\Request;
use PDF;
//for printing PDF
use LaravelDaily\Invoices\Invoice;
use LaravelDaily\Invoices\Classes\Buyer;
use LaravelDaily\Invoices\Classes\InvoiceItem;

class BillingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function bakery()
    {
        return view('admin.billing.bakery');
    }
    public function restaurant()
    {
        $orders = Order::with('product')->whereNull('status')->where('order_action','unpaid')->get();
        // dd($orders);
        return view('admin.billing.restaurant',compact('orders'));
    }
    public function bill(Request $request) 
    {
        $id = $request->bill_order_status;
        $orders = Order::with('product')->where('id',$id)->get();
        return view('admin.billing.bill',compact('orders'));
    }
    public function paid(Request $request) 
    {
        $id = $request->bill_order_status;
        Order::where('id',$id)->update(array('order_action'=>"paid"));
        return redirect()->route('admin.billing.restaurant');
    }
    public function printview($id)
    {
        $orders = Order::with('product')->where('id',$id)->get();
        $customer_name = null;
        $customer_address = null;
        $product_price[] = null;
        $product_name[] = null;
        $product_qty[] = null;
        $data = null;
        
        foreach($orders as $order)
        {
            $customer_name = $order->name;    
            $customer_address = $order->delivery_address;             
        }
        $items = array();
        
        foreach ($order->product as $key=>$product) {
            $items[] = (new InvoiceItem())->title($product->name)->pricePerUnit($product->price)->quantity($product->pivot->qty);
        }
        
        $customer = new Buyer([

            'name'          => $customer_name,
            'custom_fields' => [
                'address' => $customer_address,
            ],
        ]);

        $invoice = Invoice::make()
            ->buyer($customer)
            // ->discountByPercent(0)
            // ->taxRate(15)
            ->dateFormat('m/d/Y')
            ->currencyCode('RS')
            ->addItems($items);
        return $invoice->stream();
    }
    public function sales()
    {
        $orders = [];
        return view('admin.sales.index',compact('orders'));
    }
    public function salesdata(Request $request)
    {
        $source_date = $request->source_date;
        $destination_date = $request->destination_date;
        $orders = Order::with('product')->whereBetween('created_at',[$source_date,$destination_date])->get();
        return view('admin.sales.index',compact('orders'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
