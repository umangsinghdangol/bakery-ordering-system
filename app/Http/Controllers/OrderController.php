<?php

namespace App\Http\Controllers;

use App\Http\Requests\OrderRequest;
use App\Models\Cart;
use App\Models\Order;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $orders = Order::with('product')->whereNull('status')->where('phone_number','!=','9898989898')->get();
        return view('order.index',compact('orders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(OrderRequest $request)
    {
        DB::beginTransaction();
        try {
            if (session()->has('cart')) {
                $cart = new Cart(session()->get('cart'));
            } else {
                $cart = null;
            }
            $order = Order::create($request->all());
            foreach ($cart->items as $product) {
                $order->product()->attach($product['id'], ['qty' => $product['qty'], 'totalPrice' => $product['qty'] * $product['price']]);
            }
            DB::commit();
        }
        catch(\Exception $e){
            DB::rollBack();
        }
        Session::flush();
     return redirect()->route('master.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showdelivery()
    {
        $orders = Order::with('product')->where('status','delivery')->get();
        return view('order.delivery',compact('orders'));
    }
    public function showpickup()
    {
        $orders = Order::with('product')->where('status','pickup')->get();
        return view('order.pickup',compact('orders'));

    }
    public function statusupdate(Request $request ,$id)
    {
        $order_status_pickup=$request->order_status_pickup;
        $order_status_delivery=$request->order_status_delivery;
        if ($order_status_pickup !=null){
            Order::where('id',$id)->update(array('status'=>$order_status_pickup));
        }
        else
        {
            Order::where('id',$id)->update(array('status'=>$order_status_delivery));
        }
     //   Order::where('id',$id)->update(array('status'=>$order_status_id));


       // $data = array('status',$order_status_id);

        //DB::table('orders')->where('id','=',$id)->update($data);
       // $orders = Order::with('product')->get();
        return redirect()->route('order.index');

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function adminorder()
    {
        //
        $data['products'] = Product::where('status',1)->orderBy('id','desc')->get();
        // $category_products = DB::table('products')->join('categories','categories.id','=','category_id')->where('type','restaurant')->get()->toArray();
        return view('order.Restaurant.order',$data);
    }
    public function adminstore(OrderRequest $request)
    {

        $data = $request->all();
        $lastid = DB::table('orders')->where('name',$request->name)->where('order_action','unpaid')->first();
        if($lastid == NULL) {
            $lastid = Order::create($data)->id;
            $product_id = $request->product_id;
            $data_totalPrice = DB::table('products')->where('id',$product_id)->first();
            $qty = $request->admin_order_qty;
            $totalPrice = $qty * $data_totalPrice->price;
            $data2 = array(
                'order_id' => $lastid,
                'product_id' => $product_id,
                'qty' => $qty,
                'totalPrice' => $totalPrice,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            );
            DB::table('order_product')->insert($data2);
        }
        else {
            $product_id = $request->product_id;
            $data_totalPrice = DB::table('products')->where('id',$product_id)->first();
            $qty = $request->admin_order_qty;
            $totalPrice = $qty * $data_totalPrice->price;
            $data2 = array(
                'order_id' => $lastid->id,
                'product_id' => $product_id,
                'qty' => $qty,
                'totalPrice' => $totalPrice,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            );
            DB::table('order_product')->insert($data2);
        }
        
        return redirect()->route('admin.order');
        
    }
    public function adminunpaidlist() 
    {
        $orders = Order::with('product')->where('phone_number','9898989898')->where('order_action','unpaid')->get();
        return view('order.Restaurant.list',compact('orders'));
    }
    public function adminpaidlist() 
    {
        $orders = Order::with('product')->where('phone_number','9898989898')->where('order_action','paid')->get();
        return view('order.Restaurant.paidlist',compact('orders'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
