<?php

namespace App\Http\Controllers;
use App\Models\Cart;
use App\Models\Product;
use Illuminate\Http\Request;

class CartController extends Controller
{
    public function addToCart(Product $product){
        if( session()->has('cart')){
            $cart = new Cart(session()->get('cart'));
        } else{
            $cart = new Cart();
        }
        $cart->add($product);
        //dd($cart);
        session()->put('cart',$cart);
        return redirect()->route('master.index')->with('success','Product was added');
    }

    public function showCart(){
        if (session()->has('cart')){
            $cart = new Cart(session()->get('cart'));
        } else{
            $cart = null;
        }
        return view('cart.show',compact('cart'));
    }

    public function editCart($productid){
        if (session()->has('cart')){
            $cart = new Cart(session()->get('cart'));
        } else{
            $cart = null;
        }
        return view('cart.edit',compact('cart','productid'));
    }

    public function checkout(){
        if (session()->has('cart')){
            $cart = new Cart(session()->get('cart'));
        } else{
            $cart = null;
        }
        return view('cart.checkout',compact('cart'));
    }

    public function destroy(Product $product){
        $cart = new Cart( session()->get('cart'));
        $cart->remove($product->id);

        if( $cart->totalQty <=0){
            session()->forget('cart');
        } else{
            session()->put('cart', $cart);
        }
        return redirect()->route('cart.show')->with('success','Product was removed');
    }


    public function update(Request $request, Product $product, $status=0){
       $cart = new Cart(session()->get('cart'));
       $cart->updateQty($product->id, $request->qty);
       session()->put('cart',$cart);
       if($status == 0){
           return redirect()->route('cart.show')->with('success','Product updated');
       }
       if($status == 1){
           return redirect()->route('cart.checkout')->with('success','Product updated');
       }


    }
}
