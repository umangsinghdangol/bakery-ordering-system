<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductRequest;
use App\Models\Category;
use App\Models\Product;
use Faker\Provider\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::latest()->paginate(5);
        $categories = Category::all();
        return view('product.index', compact('products'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::where('status',1)->get();
        return view('product.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductRequest $request)
    {
        
        if (!empty($request->file('product_image'))) {
            $path = base_path() . '/public/image/product/';
            $image = $request->file('product_image');
            $name = uniqid() . '_' . $image->getClientOriginalName();

            if ($image->move($path, $name)) {
                $request->request->add(['image' => $name]);
            }
        } else {
            $request->request->add(['image' => "noimage.png"]);
        }
        Product::create($request->all());
        return redirect()->route('product.index')->with('success', 'Product inserted successfully');

    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        return view('product.show', compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        $categories = Category::all();
        return view('product.edit', compact('product', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProductRequest $request, Product $product)
    {
        if (!empty($request->file('product_image'))) {
            $path = public_path() . '/image/product/';
            $image = $request->file('product_image');
            $name = uniqid() . '_' . $image->getClientOriginalName();
            if ($image->move($path, $name)) {
                if (file_exists($path . $product->image))
                    unlink($path . $product->image);

                $request->request->add(['image' => $name]);
            }
            $product->update($request->all());
        }
        $product->update($request->all());
        return redirect()->route('product.index')->with('success', 'Product updated successfully');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $image_all=product::where('id',$product->id)->first();
        $image=$image_all->image;
        if ($image != "noimage.png"){
            $image_path=public_path('image/product/'.$image);
            $img=unlink($image_path);
            if ($img=TRUE){
                $product->delete();
                return redirect()->route('product.index')->with('error', 'Product deleted successfully');
            }
        }
        else{
            $product->delete();
            return redirect()->route('product.index')->with('error', 'Product deleted successfully');
        }
    }

}
