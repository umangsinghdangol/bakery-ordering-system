<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Gallery;
use Illuminate\Http\Request;
use DB;
use File;

class FrontendController extends Controller
{
    public function index(){
        $data['products'] = Product::where('status',1)->orderBy('id','desc')->get();
        return view('bakery.master',$data);
    }
    public function restaurant()
    {
        // $category_products = DB::table('products')->join('categories','categories.id','=','category_id')->where('type','restaurant')->get();
        $data['categories'] = Product::select('category_id')->groupBy('category_id')->get();
        $data['products'] = Product::where('status',1)->orderBy('id','desc')->get();
        return view('bakery.shop.restaurant',$data);
    }
    public function shop(){
        // $category_products = DB::table('products')->join('categories','categories.id','=','category_id')->where('type','Bakery')->get()->toArray();
        // // // dd($checks);
        $data['categories'] = Product::select('category_id')->groupBy('category_id')->get();
        $data['products'] = Product::where('status',1)->orderBy('id','desc')->get();
        return view('bakery.shop.category',$data);
    }
    public function productList(){
        $data['categories'] = Product::select('category_id')->groupBy('category_id')->get();
        $data['products'] = Product::where('status',1)->orderBy('id','desc')->get();
        return view('bakery.shop.productlist',$data);
    }
    public function categoryWiseProduct($category_id){
        $data['categories'] = Product::select('category_id')->groupBy('category_id')->get();
        $data['products'] = Product::where('category_id',$category_id)->where('status',1)->orderBy('id','desc')->get();
        return view('bakery.shop.category-wise-product',$data);
    }
    public function gallery()
    {
        $galleries = DB::table('galleries')->get();
        return view('bakery.gallery',compact('galleries'));
    }
    public function listgallery()
    {
        $galleries = DB::table('galleries')->get();
        return view('admin.gallery.list',compact('galleries'));
    }
    public function admingallery()
    {
        $galleries = DB::table('galleries')->get();
        return view ('admin.gallery.create',compact('galleries'));
    }
    public function storegallery(Request $request)
    {
        // dd($request->gallery_image);
        $this->validate($request, [
            'gallery_image' => 'required'
        ]);
        if (!empty($request->file('gallery_image'))) {
            $path =  public_path('image/gallery/');
            // dd($path);
            $image = $request->file('gallery_image');
            $name = uniqid() . '_' . $image->getClientOriginalName();
            // $file->move('images/',$filename);
            if ($image->move('image/gallery', $name)) {
                $data = array('gallery_image'=>$name,'created_at'=>date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s'));
                DB::table('galleries')->insert($data);
            }
        } 
        return view ('admin.gallery.create');
    }
    public function deletegallery(Request $request, $id)
    {
        $name_image = $request->gallery_image_name;
        $image_path = public_path('image/gallery/'.$name_image);
        if(unlink($image_path)) {
            File::delete($image_path);
            DB::table('galleries')->where('id',$id)->delete();
        }
        return redirect()->route('admin.gallery.list');
    }
}
