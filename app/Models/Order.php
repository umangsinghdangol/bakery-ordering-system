<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'orders';
    protected  $fillable =['name','email','delivery_address','phone_number','delivery_type','delivery_time','order_action'];

    public function product(){
        return $this->belongsToMany(Product::class)
            ->withPivot('qty','totalPrice');
    }

}
