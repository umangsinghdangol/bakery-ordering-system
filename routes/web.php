<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'FrontendController@index')->name('master.index');
Route::get('/restaurant', 'FrontendController@restaurant')->name('bakery.front.restaurant');
Route::get('/shop', 'FrontendController@shop')->name('bakery.front.shop');
Route::get('/product-list', 'FrontendController@productlist')->name('products.list');
Route::get('/product-category/{category_id}', 'FrontendController@categoryWiseProduct')->name('category.wise.product');
//for gallery
Route::get('/gallery', 'FrontendController@gallery')->name('bakery.gallery');
Route::get('/about',function (){
    return view('bakery.about');
});

Route::get('/addToCart/{product}', 'CartController@addToCart')->name('cart.add');
Route::get('/shopping-cart', 'CartController@showCart')->name('cart.show');
Route::get('/editing-cart/{id}', 'CartController@editCart')->name('cart.edit');
Route::post('/removing-cart/{product}', 'CartController@destroy')->name('cart.remove');
Route::put('/updating/{product}/{status}', 'CartController@update')->name('cart.update');
Route::get('/checkout', 'CartController@checkout')->name('cart.checkout');
Route::post('/order', 'OrderController@store')->name('order.store');
Route::get('/ordershow', 'OrderController@index')->name('order.index')->middleware('auth');
Route::post('/updatestatus/{id}','OrderController@statusupdate')->name('order.update.status')->middleware('auth');

//Route::post('/customer', 'CustomerController@store')->name('customer.store')->middleware('auth');
//Route::get('/customershow', 'CustomerController@index')->name('home')->middleware('auth');
Route::get('/showdelivery', 'OrderController@showdelivery')->name('order.delivery')->middleware('auth');
Route::get('/showpickup', 'OrderController@showpickup')->name('order.pickup')->middleware('auth');





Auth::routes();

Route::get('/home', 'HomeController@index')->name('home')->middleware('auth');
Route::post('/customer', 'HomeController@store')->name('customer.store')->middleware('auth');

Route::group(['middleware'=>'auth'],function (){
    Route::resource('category', 'CategoryController');
    Route::resource('product', 'ProductController');
    Route::get('/billing-bakery','BillingController@bakery')->name('admin.billing.bakery');
    Route::get('/billing-restaurant','BillingController@restaurant')->name('admin.billing.restaurant');
    Route::get('/bill','BillingController@bill')->name('admin.bakery.bill');
    Route::get('/paid','BillingController@paid')->name('admin.bakery.paid');
    Route::get('/printview/{id}', 'BillingController@printview')->name('admin.billing.print');
    Route::get('/sales','BillingController@sales')->name('admin.sales');
    Route::get('/sales-date','BillingController@salesdata')->name('admin.sales.data');
    //admin Order form
    Route::get('/adminorder','OrderController@adminorder')->name('admin.order');
    Route::post('/adminstore','OrderController@adminstore')->name('admin.store');
    Route::get('/adminunlist','OrderController@adminunpaidlist')->name('admin.list');
    Route::get('/adminlist','OrderController@adminpaidlist')->name('admin.paid.list');
    //gallery
    Route::get('/admin-gallery','FrontendController@admingallery')->name('admin.gallery');
    Route::post('/admin-gallery-store','FrontendController@storegallery')->name('admin.gallery.store');
    Route::get('/admin-gallery-list','FrontendController@listgallery')->name('admin.gallery.list');
    Route::post('/admin-gallery-delete/{id}','FrontendController@deletegallery')->name('admin.gallery.delete');


});
Route::get('register',function (){
    return abort(404);
})->middleware('auth');

