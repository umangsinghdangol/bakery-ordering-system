<!DOCTYPE html>
<html lang="en">
@include('bakery.header')
<body>
@include('bakery.nav')
<section id="shop">
    <div class="bg">
        <div class="bg-shadow">
            <div class="heading">
                <h1>Shop</h1>
                <p>{{config('app.name')}}::Bakery</p>
            </div>
        </div>
    </div>

    {{-- <div class="options">
        <li class="btn active"><a href="{{route('products.list')}}">All</a></li>
        @foreach($categories as $category)
            @if ( $category['category']['type'] == "Bakery")
                <li class="btn"><a href="{{route('category.wise.product',$category->category_id)}}">{{$category['category']['name']}}</a></li>
            @endif
        @endforeach
    </div>  --}}
     {{-- {{dd(count($category_products))}} --}}
    <div class="shop-container">
        @forelse($products as $product)
            @foreach ($categories as $category)
               
                
                @if ($category['category']['type'] == "Bakery" && $category['category']['id'] == $product->category_id)
                    <div class="product">
                        <img src="{{asset('/image/product/'.$product->image)}}" alt="">
                        <h4>{{$product->name}}</h4>
                        <p>Rs {{$product->price}}</p>
                        <a href="{{route('cart.add',$product->id)}}">Order Now</a>
                    </div>
                @endif
            @endforeach
        @empty
            <p class="blank_data">There are no products available!!</p>
        @endforelse
    </div>
</section>
@include('bakery.footer')
<script src="{{asset('bakery/app.js')}}"></script>
</body>
</html>
