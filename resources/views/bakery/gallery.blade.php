<!DOCTYPE html>
<html lang="en">
@include('bakery.header')
<body>
@include('bakery.nav')
<section id="shop">
    <div class="bg">
        <div class="bg-shadow">
            <div class="heading">
                <h1>Gallery</h1>
                <p>{{config('app.name')}}::Gallery</p>
            </div>
        </div>
    </div>
    <div class="container-fluid tm-container-content tm-mt-60 galleries_container">
        <div class="row tm-mb-90 tm-gallery rows_galleries">
            {{-- Image --}} 
            @forelse ($galleries as $gallery)
                <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-12 mb-5 galleries">
                    <figure class="effect-ming tm-video-item figure_galleries">
                        <a href="{{asset('image/gallery/'.$gallery->gallery_image)}}"> 
                            <img src="{{asset('image/gallery/'.$gallery->gallery_image)}}" alt="Image" class="img-fluid" >
                        </a>
                    </figure>
                    
                </div>
            @empty
                <p class="blank_data">There are no Image in Gallery</p>
            @endforelse
        </div> 
    </div> 




</section>
@include('bakery.footer')
<script src="{{asset('bakery/app.js')}}"></script>

</script>
</body>
</html>
