<footer>
    <div class="about">
        <div class="about-container">
            <h1>Cafe Jacobs</h1>
            <p>Jacob's, which was initially registered in year 2066, and now serving for more than a decade in bakery 
            and restaurant services. Jacob's is unique with it's bakery and food items. 
            <!-- Jacob's could well be one of the 
            finest in terms of process automation one can see in bakery industry inside Chitwan district. Jacob's is 
            the town best restaurant for family and friends. -->
            Moreover, purpose of providing quality bakery and restaurant 
            service is main stream of Jacob's cafe.
            </p>
        </div>
        <div class="links">
            <h1>Quick Links</h1>
            <a href="{{route('master.index')}}">Home</a>
            {{-- <a href="{{url('about')}}">About</a> --}}
            <a href="{{route('bakery.gallery')}}">Gallery</a>
            <a href="{{route('bakery.front.shop')}}">Bakery</a>
            <a href="{{route('bakery.front.restaurant')}}">Restaurant</a>
        </div>
        <div class="touch">
            <h1>Get in touch</h1>
            <i class="fas fa-map-marker-alt"></i>&nbsp;<span>Bharatpur, Chitwan</span><br>
            <i class="fas fa-phone-alt"></i>&nbsp;<span>01-56531197</span><br>
            <i class="far fa-envelope"></i>&nbsp;<span>bakery@shop.com</span>
        </div>
    </div>
    <h3>Copyright &copy; 2020. All rights Reserved</h3>
</footer>
