<!DOCTYPE html>
<html lang="en">
@include('bakery.header')
<body>
@if($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach($errors->all() as $error)
                <li>{{$error}}</li>
            @endforeach
        </ul>
    </div>
@endif
@include('bakery.nav')
<main>
    <!-- slider -->
@include('bakery.slider')
<!-- slider end -->

    <!-- advertising banner-->
    <section id="menu">
        <div class="container">
            <div class="restaurent">
                <div class="bg">
                    <div class="bg-shadow">
                        <div class="items">
                            <h1>Healthy and tasty fast food</h1>
                            <a href="{{route('bakery.front.restaurant')}}">Order Now</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="bakery">
                <div class="bg">
                    <div class="bg-shadow">
                        <div class="items">
                            <h1>Tasty and Delicious bakery items</h1>
                            <a href="{{route('bakery.front.shop')}}">Order Now</a>
                        </div>
                    </div>
                </div>

            </div>
    </section>
    <!-- advertising end  -->

    <!-- purchase banner -->
    <section id="purchase">
        <div class="bg">
            <div class="bg-shadow">
                <div class="content">
                    <h1>Looking for delicious Cakes? </h1>
                    <a href="{{route('bakery.front.shop')}}">Order Now</a>
                </div>
            </div>
        </div>
    </section>
    <!-- purchase banner end -->

    <!-- contact -->
    <section id="contact">
        <div class="contacts">
            <h1>Get In Touch</h1>
            <form action="{{route('customer.store')}}" method="post">
                @csrf
                <input type="text" name="name" placeholder=" Your Name"><br>
                <input type="email" name="email" placeholder="Your Email"><br>
                <input type="submit" value="Send">
            </form>
        </div>
    </section>
    <!-- contacts -->
</main>
    <!-- footer section -->
@include('bakery.footer')
<!-- footer end -->


<script src="{{asset('bakery/app.js')}}"></script>
<script src="{{asset('bakery/swiper/swiper-bundle.js')}}"></script>
<script src="{{asset('bakery/swiper/swiper-bundle.min.js')}}"></script>


<!--Sticky Navbar-->
<script type="text/javascript">

</script>

<!-- slider js -->

<script>
    var mySwiper = new Swiper('.swiper-container', {
        loop:true,
        speed:3000,
        pagination: {
            el: '.swiper-pagination',
            clickable:true,
        },
        

        // Navigation arrows
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
        autoplay: {
            delay: 3000,
        },
        fadeEffect: {
            crossFade: true,
        },
        effect:'fade',
        breakpoints: {
            600: {
                slidesPerView: 1,
            },
            767: {
                slidesPerView: 1,
            },
            1024: {
                slidesPerView: 1,
            },
        }
    });
</script>
</body>
</html>
