<nav class="border-bottom-0">
    <span class="menu" id="open"><i class="fa fa-bars" aria-hidden="true" onclick="menu()"></i></span>

    <div class="logo">
       {{-- <h1> --}}
        {{-- <a href="{{route('master.index')}}"> Cafe Jacobs </a></h1> --}}
       <h1><a href="{{route('master.index')}}">Cafe Jacobs</a></h1>
    </div>
    <ul id="nav-links">
        <li><a href="{{route('master.index')}}">Home</a></li>
        <li><a href="{{url('about')}}">About</a></li>
        <li><a href="{{route('bakery.gallery')}}">Gallery</a></li>
        <li><a href="{{route('bakery.front.shop')}}">Bakery</a></li>
        <li><a href="{{route('bakery.front.restaurant')}}">Restaurant</a></li>
        
        
    </ul>
    <a href="{{route('cart.show')}}">
                <i class="fas fa-shopping-cart">({{ session()->has('cart') ? session()->get('cart')->totalQty : '0'}})
                </i>
            </a>

</nav>
