<!DOCTYPE html>
<html lang="en">
@include('bakery.header')
<body>
@include('bakery.nav')
<section id="about-us">
    <div class="bg">
        <div class="bg-shadow">
            <div class="heading">
                <h1>About Us</h1>
                <p>{{config('app.name')}}::About us</p>
            </div>
        </div>
    </div>

    <div class="about">
        <div class="about-img">
            <img src="{{asset('bakery/images/chef.jpg')}}" alt="">
        </div>
        <div class="about-container">
            <h2>Best bakery in town</h2>
            <p>Jacob's, which was initially registered in year 2066, and now serving for more than a decade in bakery 
            and restaurant services. Contrary to how bakeries and cafe begin, Jacob's began with purpose of serving 
            bakery and restaurant food by Arjun KC who have already served into different countries like South Korea, 
            Qatar and Singapore. Jacob's is unique with it's bakery and food items. Jacob's could well be one of the 
            finest in terms of process automation one can see in bakery industry inside Chitwan district. Jacob's is 
            the town best restaurant for family and friends. Moreover, purpose of providing quality bakery and restaurant 
            service is main stream of Jacob's cafe.
            </p>
        </div>
    </div>
</section>
<section id="remember">
    <div class="heading">
        <h1>What We Offer</h1>
        <p>We Provide all kinds of bakery products</p>
    </div>
    <div class="remember-container">
        <div class="product">
            <img src="{{asset('bakery/images/blackforest.jpg')}}" alt="">
            <h4>Cakes</h4>
        </div>
        <div class="product">
            <img src="{{asset('bakery/images/buns.jpg')}}" alt="">
            <h4>Bread & Buns</h4>
        </div>
        <div class="product">
            <img src="{{asset('bakery/images/chocolatechips.jpg')}}" alt="">
            <h4>Biscuits</h4>
        </div>
    </div>
</section>
@include('bakery.footer')
<script src="{{asset('bakery/app.js')}}"></script>
</body>
</html>
