@extends('layouts.backend')
@section('title','List Category')
@section('content')
@section('css')
@endsection
@section('js')
@endsection
<section class="content-header">
    <h1>
        Customer Management
    </h1>
</section>
<section class="content">
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">List Customer
            </h3>
        </div>

        @if(session()->has('success'))
            <div class="alert alert-success">
                {{session()->get('success')}}
            </div>
        @endif

        @if(session()->has('error'))
            <div class="alert alert-danger">
                {{session()->get('error')}}
            </div>
        @endif

        <div class="box-body">
            <table class="table table-bordered text-center" id="category_table">
                <thead >
                <tr>
                    <th>SN</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Date</th>
                </tr>
                </thead>
                <tbody>
                @php($i=1)
                @foreach($customers as $customer)
                    <tr>
                        <td>{{$i++}}</td>
                        <td>{{$customer->name}}</td>
                        <td>{{$customer->email}}</td>
                        <td>{{$customer->created_at}}</td>
                    </tr>
                @endforeach
                </tbody>
                <tfoot>
                </tfoot>
            </table>
        </div>
    </div>
</section>
@endsection
