@extends('layouts.backend')
@section('title','List Order')
@section('content')
@section('css')
@endsection
@section('js')
@endsection
<section class="content-header">
    <h1>
        Restaurant Order Management
    </h1>
</section>
<section class="content">
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">List of Paid data
            </h3>
        </div>

        @if(session()->has('success'))
            <div class="alert alert-success">
                {{session()->get('success')}}
            </div>
        @endif

        @if(session()->has('error'))
            <div class="alert alert-danger">
                {{session()->get('error')}}
            </div>
        @endif

        <div class="box-body">
            <table class="table table-bordered" id="category_table">
                <thead>
                <tr>
                    <th>SN</th>
                    <th>Date</th>
                    <th>Table No.</th>
                    <th>Product Name</th>
                    <th>Product Qty</th>
                    <th>Rate</th>
                    <th>Total Amount</th>
                </tr>
                </thead>
                <tbody>
                @php($i=1)
                @forelse($orders as $order)
                        <tr>
                            <td>{{$i++}}</td>
                            <td>{{$order->created_at}}</td>
                            <td>{{$order->name}}</td>
                            <td>
                                <table>
                                    @foreach($order->product as $product)
                                        <tr>
                                            <td>{{$product->name}}</td>
                                        </tr>
                                    @endforeach
                                </table>
                            </td>

                            <td>
                                <table>
                                    @foreach($order->product as $product)
                                        <tr>
                                            <td>{{$product->pivot->qty}}</td>
                                        </tr>
                                    @endforeach
                                </table>
                            </td>

                            <td>
                                <table>
                                    @foreach($order->product as $product)
                                        <tr>
                                            <td>{{$product->price}}</td>
                                        </tr>
                                    @endforeach
                                </table>
                            </td>

                            <td>{{$order->product()->sum('totalPrice')}}</td>
                        </tr>

                @empty
                    <p class="blank_data">There are no paid data!!</p>
                @endforelse

                </tbody>
            </table>
        </div>
    </div>
</section>
@endsection
