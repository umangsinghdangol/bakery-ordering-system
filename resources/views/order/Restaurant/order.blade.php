@extends('layouts.backend')
@section('title','List Order')
@section('content')
@section('css')
@endsection
@section('js')
@endsection
<section class="content-header">
    <h1>
        Restaurant Food Ordering
    </h1>
</section>
<section class="content">
    <div class="container">
        <div class="row">
            <div class="col-md-offset-3 col-md-6 col-sm-6 col-xs-12 form-box">
                <h1 class="text-center">Restaurant order Form</h1> <hr>
                @if($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{$error}}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form class="form-horizontal" role="form" action="{{route('admin.store')}}" method="post">
                    @csrf
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-3" for="name">Table No: </label>
                        <div class="col-md-8 col-sm-8 col-xs-8">
                            <select name="name" id="">
                                <option value="">Select</option>
                                <option value="Table No. 1">Table No. 1</option>
                                <option value="Table No. 2">Table No. 2</option>
                                <option value="Table No. 3">Table No. 3</option>
                                <option value="Table No. 4">Table No. 4</option>
                                <option value="Table No. 5">Table No. 5</option>
                                <option value="Table No. 6">Table No. 6</option>
                                <option value="Table No. 7">Table No. 7</option>
                                <option value="Table No. 8">Table No. 8</option>
                                <option value="Table No. 9">Table No. 9</option>
                                <option value="Table No. 10">Table No. 10</option>
                                <option value="Table No. 11">Table No. 11</option>
                                <option value="Table No. 12">Table No. 12</option>
                                <option value="Order Delivery">Order Delivery</option>
                            </select>
                        </div>
                    </div>
                    <input type="hidden" class="form-control" id="email" value="customer@gmail.com" name="email" readonly>
                    <input type="hidden" name="delivery_address" value="Bharatpur" readonly>
                    <input type="hidden"  name="phone_number" value="9898989898" readonly>
                    <input type="hidden"  name="delivery_type" value="0" readonly>
                    <input type="hidden"  name="order_action" value="unpaid" readonly>
                    <input type="hidden"   name="delivery_time" value="{{ date('m/d/Y H:i:s') }}" readonly>
                    <input type="hidden"   name="status" value="restaurant" readonly>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-3" for="menu">Menu</label>
                        <div class="col-md-8 col-sm-8 col-xs-8">
                            <div class="input-group control-group after-add-more">
                                <div class="classname">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-3" name="admin_product_name">Product Name</label>
                                    <select name="product_id">
                                        <option value="Select">Select</option>
                                        @forelse ($products as $product)
                                            <option value="{{$product->id}}">{{$product->name}}</option>
                                        @empty
                                            <option value="select">No item</option>
                                        @endforelse
                                    </select>
                                </div><br>
                                <div class="classname">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-3">Quantity</label>
                                    <input type="number" name="admin_order_qty" >
                                </div>
                                {{-- For Multiply Order --}}
                                {{-- <div class="input-group-btn"> 
                                    <button class="btn btn-success add-more" type="button"><i class="glyphicon glyphicon-plus"></i></button>
                                </div> --}}
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-6 col-md-offset-3 col-sm-offset-3 col-xs-offset-3 text-center">
                            <button type="submit" class="btn btn-success btn-block submit">Order</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>	
    </div>
    
</section>
@endsection
