@extends('layouts.backend')
@section('title','List Order')
@section('content')
@section('css')
@endsection
@section('js')
@endsection
<section class="content-header">
    <h1>
        Order Management
    </h1>
</section>
<section class="content">
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">List Order
            </h3>
        </div>

        @if(session()->has('success'))
            <div class="alert alert-success">
                {{session()->get('success')}}
            </div>
        @endif

        @if(session()->has('error'))
            <div class="alert alert-danger">
                {{session()->get('error')}}
            </div>
        @endif

        <div class="box-body">
            <table class="table table-bordered" id="category_table">
                <thead>
                <tr>
                    <th>SN</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Address</th>
                    <th>Phone Number</th>
                    <th>Delivery Type</th>
                    <th>Date</th>
                    <th>Product Name</th>
                    <th>Product Qty</th>
                    <th>Rate</th>
                    <th>Total Amount</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @php($i=1)
                @forelse($orders as $order)
                        <tr>
                            <td>{{$i++}}</td>
                            <td>{{$order->name}}</td>
                            <td>{{$order->email}}</td>
                            <td>{{$order->delivery_address}}</td>
                            <td>{{$order->phone_number}}</td>
                            <td>
                                @if($order->delivery_type ===1)
                                    <label>Pick up</label>
                                @else
                                    <label>delivery</label>
                                @endif
                            </td>
                            <td>{{$order->delivery_time}}</td>
                            <td>
                                <table>
                                    @foreach($order->product as $product)
                                        <tr>
                                            <td>{{$product->name}}</td>
                                        </tr>
                                    @endforeach
                                </table>
                            </td>

                            <td>
                                <table>
                                    @foreach($order->product as $product)
                                        <tr>
                                            <td>{{$product->pivot->qty}}</td>
                                        </tr>
                                    @endforeach
                                </table>
                            </td>

                            <td>
                                <table>
                                    @foreach($order->product as $product)
                                        <tr>
                                            <td>{{$product->price}}</td>
                                        </tr>
                                    @endforeach
                                </table>
                            </td>

                            <td>
                                <table>

                                    @foreach($order->product as $product)
                                        <tr>
{{--                                            {{dd($product->pivot->qty*$product->price)}}--}}
{{--                                            @php($total=$product->pivot->qty*$product->price+$total)--}}
                                            <td>{{$product->pivot->qty*$product->price}}</td>
                                        </tr>
                                    @endforeach
                                </table>
                            </td>
                            <td>
                                <form action="{{route('order.update.status',$order->id)}}" method="post">
                                    @csrf
                                    @if($order->delivery_type ===1)
                                        <input type="hidden" value="pickup" name="order_status_pickup">
                                    @else
                                        <input type="hidden" value="delivery" name="order_status_delivery">
                                    @endif

                                    <input type="hidden" value="{{$order->id}}" id="order_status">
                                    <button type="submit">
                                        @if($order->delivery_type ===1)
                                            pickup
                                        @else
                                            delivery
                                        @endif
                                    </button>
                                </form>
                            </td>
                        </tr>
                @empty
                    <p class="blank_data">There are no list of orders!!</p>
                @endforelse

                </tbody>
            </table>
        </div>
    </div>
</section>
@endsection
