@extends('layouts.backend')
@section('title','Create Product')
@section('content')
    <section class="content-header">
        <h1>
            Product Management
        </h1>
    </section>>
    <section class="content">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Edit Product</h3>
                <a href="{{route('product.index')}}" class="btn btn-info"><i class="fa fa-list"></i>List</a>
            </div>
            <div class="box-body">
                @if($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{$error}}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form action="{{route('product.update',$product->id)}}" method="post" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <div class="form-group">
                        <label for="category_id">Category Id</label>
                        <select name="category_id" class="form-control">
                            <option>Select Category</option>
                            @foreach($categories as $category)
                                @if($category->id == $product->category_id)
                                    <option value="{{$category->id}}"selected>{{$category->name}}</option>
                                @else
                                    <option value="{{$category->id}}">{{$category->name}}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" name="name" class="form-control" id="name" value="{{$product->name}}">
                    </div>
                    <div class="forms-group">
                        <label for="price">Price</label>
                        <input type="number" name="price" class="form-control" value="{{$product->price}}">
                    </div>
{{--                    <img src="{{asset('image/product/' .$product->image )}}" width="100" alt="">--}}
                    <div class="forms-group">
                        <label for="image">Image</label>
                        <input type="file" name="product_image" class="form-control" value="{{$product->image}}">
                    </div>
                    <div class="form-group">
                        <label for="quantity">Quantity</label>
                        <input type="text" name="quantity" id="quantity" class="form-control" placeholder="Enter quantity" value="{{$product->quantity}}">
                    </div>
                    <div class="form-group">
                        <label for="active">Status</label>
                        @if($product->status == 1)
                            <input type="radio" id="active" name="status"  value="{{$product->status}}" checked>Active
                            <input type="radio" id="deactive" name="status" value="0">De Active
                        @else
                            <input type="radio" id="active" name="status"  value="1">Active
                            <input type="radio" id="deactive" name="status" value="{{$product->status}}" checked>De Active
                        @endif
                    </div>

                    <div class="form-group">
                        <input type="submit" name="save" value="Save Product" class="btn btn-success"/>
                        <input type="submit" name="clear" value="Clear" class="btn btn-danger"/>
                    </div>



                </form>
            </div>
        </div>
    </section>
@endsection
