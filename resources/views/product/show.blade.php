@extends('layouts.backend')
@section('title','Show Product')
@section('content')
    <section class="content-header">
        <h1>
            Product Management
        </h1>
    </section>
    <section class="content">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Show product</h3>
                <a href="{{route('product.create')}}" class="btn btn-info"><i class="fa fa-plus"></i>Create</a>
                <a href="{{route('product.index')}}" class="btn btn-info"><i class="fa fa-plus"></i>List</a>
            </div>
            <div class="box-body">
                <table class="table table-border">
                    <tr>
                        <th>Category Name</th>
                        <td>{{$product->category->name}}</td>
                    </tr>
                    <tr>
                        <th>Name</th>
                        <td>{{$product->name}}</td>
                    </tr>
                    <tr>
                        <th>Price</th>
                        <td>{{$product->price}}</td>
                    </tr>
                    <tr>
                        <th>Image</th>
                        <td>{{$product->image}}</td>
                        <div class="img-wrap">
                            <span class="close">&times;</span>
                            <img class="image" src="{{asset('image/product/'. $product->image )}}" alt={{$product->image}} height="100px" width="100px" style="cursor:pointer">
                        </div>

                    </tr>
                    <tr>
                        <th>Quantity</th>
                        <td>{{$product->quantity}}</td>
                    </tr>
                    <tr>
                        <th>Status</th>
                        <td>
                            @if($product->status == 1)
                                <label class="label label-success">Active</label>
                            @else
                                <label class="label label-danger">De Active</label>
                            @endif
                        </td>

                    </tr>
                    <tr>
                        <th>Created At</th>
                        <td>{{$product->created_at}}</td>
                    </tr>
                    <tr>
                        <th>Updated At</th>
                        <td>{{$product->updated_at}}</td>
                    </tr>
                </table>
            </div>
        </div>
    </section>
@endsection
