@extends('layouts.backend')
@section('title','List Product')
@section('content')
    <section class="content-header">
        <h1>
            Product Management
        </h1>
    </section>
    <section class="content">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Lists product</h3>
                <a href="{{route('product.create')}}" class="btn btn-info"><i class="fa fa-plus"></i>Create</a>
            </div>
            @if(session()->has('success'))
                <div class="alert alert-success">
                    {{session()->get('success')}}
                </div>
            @endif

            @if(session()->has('error'))
                <div class="alert alert-danger">
                    {{session()->get('error')}}
                </div>
            @endif

            <div class="box-body">
                <table class="table table-border text-center">
                    <thead >
                    <tr >
                        <th>SN</th>
                        <th>Category</th>
                        <th>Name</th>
                        <th>Type</th>
                        <th>Price</th>
                        <th>Image</th>
                        <th>Quantity</th>
                        <th>Status</th>
                        <th>Action</th>
                        <th>Delete</th>
                    </tr>
                    </thead>
                    <tbody class="my-auto">
                    @php($i=1)
                    @foreach($products as $product)
                        <tr>
                            <td>{{$i++}}</td>
                            <td>{{$product->category->name}}</td>
                            <td>{{$product->name}}</td>
                            <td>{{$product->category->type}}</td>
                            <td>{{$product->price}}</td>
                            <td><img src="{{asset('image/product/' .$product->image )}}" width="30" height="30" alt=""></td>
                            <td>{{$product->quantity}}</td>
                            <td>
                                @if($product->status == 1)
                                    <label class="label label-success">Active</label>
                                @else
                                    <label class="label label-danger">De active</label>
                                @endif
                            </td>
                            <td>
                                <a href="{{route('product.edit',$product->id)}}" class="btn btn-warning"><i class="fa fa-pencil"></i></a>
                                <a href="{{route('product.show',$product->id)}}" class="btn btn-info"><i class="fa fa-eye"></i></a>
                            </td>
                            <td>
                                <form action="{{route('product.destroy',$product->id)}}" method="post"  onsubmit="return confirm('Are you sure to delete')">
                                    @csrf
                                    <input type="hidden" name="_method" value="DELETE"/>
                                    <button class="btn btn-danger"> <i class="fa fa-trash"></i></button>
                                </form>

                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div class="d-flex justify-content-center align-items-center">
                    <div>{{$products->links()}}</div>
                </div>
            </div>
        </div>
    </section>
@endsection
