@extends('layouts.backend')
@section('title','Create Product')
@section('content')
    <section class="content-header">
        <h1>
            Product Management
        </h1>
    </section>
    <section class="content">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Create product</h3>
                <a href="{{route('product.index')}}" class="btn btn-info"><i class="fa fa-list"></i>List</a>
            </div>
            <div class="box-body">
                @if($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{$error}}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form action="{{route('product.store')}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="forms-group">
                        <label for="category_id">Category Name</label>
                        <select name="category_id" class="form-control">
                            <option>Select Category</option>
                            @foreach($categories as $category)
                                <option value="{{$category->id}}">{{$category->name}}({{$category->type}})</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" name="name" class="form-control" id="name" placeholder="Enter the Name of Product" >
                    </div>
                    <div class="forms-group">
                        <label for="price">Price</label>
                        <input type="number" name="price" class="form-control" min="0" placeholder="Enter the Price of Price">
                    </div>
                    <div class="forms-group">
                        <label for="image">Image</label>
                        <input type="file" name="product_image">
                    </div>
                    <div class="form-group">
                        <label for="quantity">Quantity</label>
                        <input type="number" name="quantity" id="quantity" class="form-control" placeholder="Enter the Quantity of the Product" min="0">
                    </div>
                    <div class="form-group">
                        <label for="active">Status</label>
                        <input type="radio" name="status"  value="1" id="active"/>Active
                        <input type="radio" name="status"  value="0" id="deactive" checked />DeActive
                    </div>
                    <div class="form-group">
                        <input type="submit" name="save" value="Save Product" class="btn btn-success"/>
                        <input type="submit" name="clear" value="Clear" class="btn btn-danger"/>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection
