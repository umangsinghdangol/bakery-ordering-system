<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            
            
            <li class="treeview">
                <a href="{{route('admin.gallery')}}">
                    <i class="fa fa-picture-o" aria-hidden="true"></i>
                    <span>Gallery</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{route('admin.gallery')}}"><i class="fa fa-plus"></i> Create</a></li>
                    <li><a href="{{route('admin.gallery.list')}}"><i class="fa fa-list" aria-hidden="true"></i>View Gallery</a></li>
                </ul>
            </li>

            <li class="treeview">
                <a href="{{route('category.create')}}">
                    <i class="fa fa-list-alt" aria-hidden="true"></i>
                    <span>Category Management</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{route('category.create')}}"><i class="fa fa-plus"></i> Create</a></li>
                    <li><a href="{{route('category.index')}}"><i class="fa fa-list" aria-hidden="true"></i> List</a></li>
                </ul>
            </li>

            <li class="treeview">
                <a href="{{route('product.create')}}">
                    <i class="fa fa-product-hunt" aria-hidden="true"></i>
                    <span>Product Management</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{route('product.create')}}"><i class="fa fa-plus"></i> Create</a></li>
                    <li><a href="{{route('product.index')}}"><i class="fa fa-list" aria-hidden="true"></i> List</a></li>
                </ul>
            </li>

            <li class="treeview">
                <a href="{{route('order.index')}}">
                    <i class="fa fa-tasks"></i>
                    <span>Bakery Order</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{route('order.index')}}"><i class="fa fa-list" aria-hidden="true"></i> List</a></li>
                    <li><a href="{{route('order.delivery')}}"><i class="fa fa-truck" aria-hidden="true"></i>Delivery</a></li>
                    <li><a href="{{route('order.pickup')}}"><i class="fa fa-user-circle-o" aria-hidden="true"></i>Pickup</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="{{route('admin.list')}}">
                    <i class="fa fa-tasks"></i>
                    <span>Restaurant Order</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{route('admin.order')}}"><i class="fa fa-user-circle-o" aria-hidden="true"></i>Order</a></li>
                    <li><a href="{{route('admin.list')}}"><i class="fa fa-list" aria-hidden="true"></i>Unpaid</a></li>
                    <li><a href="{{route('admin.paid.list')}}"><i class="fa fa-list" aria-hidden="true"></i>Paid</a></li>
                </ul>
            </li>
            <li >
                <a href="{{ route('admin.billing.restaurant')}}">
                    {{-- <i class="fas fa-file-invoice"></i> --}}
                    <i class="fa fa-tasks"></i>
                    <span>Billing System</span>
                    {{-- <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span> --}}
                </a>
                {{-- <ul class="treeview-menu">
                    <li><a href="{{route('admin.billing.bakery')}}"><i class="fa fa-list" aria-hidden="true"></i>Bakery</a></li>
                    <li><a href="{{route('admin.billing.restaurant')}}"><i class="fa fa-truck" aria-hidden="true"></i>Restaurant</a></li>
                </ul> --}}
            </li>
            <li>
                <a href="{{ route('admin.sales')}}">
                    <i class="fa fa-user"></i>
                    <span>Sales</span>
                </a>
            </li>
            <li>
                <a href="{{ route('home')}}">
                    <i class="fa fa-user"></i>
                    <span>Customer Management</span>
                </a>
            </li>

            





        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
