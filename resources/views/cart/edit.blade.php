<!DOCTYPE html>
<html lang="en">
@include('bakery.header')
<body>
@include('bakery.nav')
<section id="about-us">
    <div class="bg">
        <div class="bg-shadow">
            <div class="heading">
                <h1>Shopping Cart</h1>
                <P>{{config('app.name')}}::Edit Shopping Cart</P>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            @if($cart)
                <div class="col-md-8">
                    @if($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach($errors->all() as $error)
                                    <li>{{ $error  }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    @foreach( $cart->items as $product)
                       @if($product['id'] == $productid)
                                <div class="card mb-2 cardss">
                                    <h2 class="card-title">
                                        {{$product['name']}}
                                    </h2>
                                    <div class="card-text">
                                        Rs{{$product['price']}}

                                        <form action="{{route('cart.update',[$product['id'],1])}}" method="post">
                                            @csrf
                                            @method('put')
                                            <input type="number" name="qty" id="qty" value="{{$product['qty']}}">
                                            <button type="submit" class="btn btn-secondary btn-sm changes">Change</button>
                                        </form>
                                        <form action="{{route('cart.remove',$product['id'])}}" method="post">
                                            @csrf
                                            <button type="submit" class="btn btn-danger removes">Remove</button>
                                        </form>
                                    </div>
                                </div>
                </div>
                           @endif
                @endforeach
                <div class="card_payment"><p><strong>Total :Rs{{$cart->totalPrice}}</strong></p></div>

{{--                <div class="col-md-4">--}}
{{--                    <div class="card bg-primary text-white">--}}
{{--                        <div class="card-body">--}}
{{--                            <h3 class="card-title">--}}
{{--                                Your Cart--}}
{{--                                <hr>--}}
{{--                            </h3>--}}
{{--                            <div class="card-text">--}}
{{--                                <p>--}}
{{--                                    Total amount is Rs{{$cart->totalPrice}}--}}
{{--                                </p>--}}
{{--                                <p>--}}
{{--                                    Total quantities is {{$cart->totalQty}}--}}
{{--                                </p>--}}
{{--                                <a href="{{route('cart.checkout')}}" class="btn btn-info">Checkout</a>--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                    </div>--}}
{{--                </div>--}}
                <a href="{{route('cart.checkout')}}" class="btn btn-info checkpay">Checkout</a>
            @else
                <p>There are no items in cart</p>
            @endif

        </div>

    </div>
</section>
@include('bakery.footer')
<script src="{{asset('bakery/app.js')}}"></script>
</body>
</html>
