<!DOCTYPE html>
<html lang="en">
@include('bakery.header')
<body>
@include('bakery.nav')
<section id="about-us">
    <div class="bg">
        <div class="bg-shadow">
            <div class="heading">
                <h1>Shopping Cart</h1>
                <p>{{config('app.name')}}::Shopping Cart</p>
            </div>
        </div>
    </div>

    @if($cart)
    @if($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{ $error  }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="grid-container">
        <div class="table-box">
        <table>
            <thead>
                <tr>
                    <td>Product Name</td>
                    <td>Price</td>
                    <td>Quantity/Action</td>
                    <td></td>
                    <td>Total</td>
                    
                </tr>
            </thead>
            <tbody>
                @foreach( $cart->items as $product)
                <tr>
                    <td>{{$product['name']}}</td>
                    <td>Rs{{$product['price']}}</td>
                    <td class="buttons">
                        <!-- <input type="number" name="qty" value="{{$product['qty']}}" min="1">	 -->
                        
                        <form action="{{route('cart.update',[$product['id'],0])}}" method="post">
                            @csrf
                            @method('put')
                            <input type="number" name="qty" value="{{$product['qty']}}" min="1">
                            <button class="change">Change</button>
                        </form>
                        
                    </td>
                    <td>
                    <form action="{{route('cart.remove',$product['id'])}}" method="post">
                            @csrf
                            <button class="remove">Remove</button>
                        </form>
                    </td>
                    <td>{{$product['price'] * $product['qty'] }}</td>
                    <!-- <td class="buttons">
                        <form action="{{route('cart.remove',$product['id'])}}" method="post">
                            @csrf
                            <button class="remove">Remove</button>
                        </form>
                        <form action="{{route('cart.update',[$product['id'],0])}}" method="post">
                            @csrf
                            @method('put')
                            <input type="number" name="qty" value="{{$product['qty']}}" min="1">
                            <button class="change">Change</button>
                        </form>
                    </td> -->
                </tr>
                @endforeach
                <tr>
                    <td colspan="3" style="text-align: right;">Total</td>
                    <td colspan="3">{{$cart->totalPrice}}</td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="cart">
        <div class="cart_box">
            <h1>Your Cart</h1>
            <p>Total amount is Rs {{$cart->totalPrice}}</p>
            <p>Total Quantity is {{$cart->totalQty}}</p>
            <button class="checkout"><a href="{{route('cart.checkout')}}">Proceed to Checkout</a></button>
        </div>
    </div>
</div>
    @else
    <div class="col-md-12">
        <p class="blank_data">There are no items in cart!!</p>
    </div>
    @endif


    {{-- <div class="container">
        <div class="row">
            @if($cart)
                <div class="col-md-8">
                    @if($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach($errors->all() as $error)
                                    <li>{{ $error  }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    @foreach( $cart->items as $product)
                        <div class="card mb-2">
                            <h5 class="card-title">
                                {{$product['name']}}
                            </h5>
                            <div class="card-text">
                                Rs{{$product['price']}}

                                <form action="{{route('cart.update',[$product['id'],0])}}" method="post">
                                    @csrf
                                    @method('put')
                                    <input type="number" name="qty" id="qty" value="{{$product['qty']}}">
                                    <button type="submit" class="btn btn-secondary btn-sm">Change</button>
                                </form>

                                <form action="{{route('cart.remove',$product['id'])}}" method="post">
                                    @csrf
                                    <button type="submit" class="btn btn-danger">Remove</button>
                                </form>
                            </div>
                        </div>
                </div>
                @endforeach
                <p><strong>Total :Rs{{$cart->totalPrice}}</strong></p>

                <div class="col-md-4">
                    <div class="card bg-primary text-white">
                        <div class="card-body">
                            <h3 class="card-title">
                                Your Cart
                                <hr>
                            </h3>
                            <div class="card-text">
                                <p>
                                    Total amount is Rs{{$cart->totalPrice}}
                                </p>
                                <p>
                                    Total quantities is {{$cart->totalQty}}
                                </p>
                                <a href="{{route('cart.checkout')}}" class="btn btn-info">Checkout</a>
                            </div>
                        </div>

                    </div>
                </div>
            @else
                <div class="col-md-12">
                    <p class="blank_data">There are no items in cart!!</p>
                </div>

            @endif

        </div>

    </div> --}}
</section>
@include('bakery.footer')
<script src="{{asset('bakery/app.js')}}"></script>
</body>
</html>
