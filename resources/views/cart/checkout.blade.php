<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Bakery Shop</title>
    <link rel="stylesheet" href="{{asset('bakery/fontawesome/fontawesome-free/css/all.css')}}">
    <link rel="stylesheet" href="{{asset('bakery/style.css')}}">
    <link href="https://code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css" rel="stylesheet" >
    <script src="https://code.jquery.com/jquery-2.2.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
        $(document).ready(function(){
            var minDate = new Date();
            $('#delivery').datepicker({
                showAnim: 'drop',
                minDate: minDate,
                dateFormat: 'dd/mm/yy',
            });
        });
    </script>
</head>
<body>
@include('bakery.nav')
<section id="order">
    <div class="bg">
        <div class="bg-shadow">
            <div class="heading">
                <h1>CheckOut</h1>
                <P>{{config('app.name')}}::CheckOut</P>
            </div>
        </div>
    </div>

    
    <div class="order-form">
    
        <div class="bakery-items">
        <h1>Ord<span>er Inform</span>ation</h1>
            @if($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            {{$error}}
                        @endforeach
                    </ul>
                </div>
            @endif
            <form action="{{route('order.store')}}" method="post">
                @csrf
                <div>
                <label>Name:</label><br>
                <input type="text" name="name">
                </div>
                <div>
                <label >Email:</label><br>
                <input type="email" name="email">
                </div>
                <div>
                <label>Delivery Address:</label><br>
                <input type="text" name="delivery_address">
                </div>
                <div>
                <label>Phone Number:</label><br>
                <input type="number" name="phone_number">
                </div>
                <div class="table">
                <table>
                    <tr>
                        <th>Items</th>
                        <th>Quantity</th>
                        <th>Rate</th>
                        <th>Total Price</th>
                        <th>Action</th>
                    </tr>
                    @foreach($cart->items as $product)
                    <tr>
                        <td>{{$product['name']}}</td>
                        <td>{{$product['qty']}}</td>
                        <td>Rs{{$product['price']}}</td>
                        <td>Rs{{$product['qty'] * $product['price']}}</td>
                        <td>
                            <a href="{{route('cart.edit',$product['id'])}}" class="btn btn-warning editing"><i class="fa fa-pencil"></i>Edit</a>
                        </td>
                    </tr>
                    @endforeach
                    <tr>
                        <td colspan="4">Total Amount</td>
                        <td>{{$cart->totalPrice}}</td>
                    </tr>
                </table>
                </div>
                <div class="pickup">
                <h1>Pick<span>up / De</span>livery</h1>
                <input type="radio" name="delivery_type" value="1">&nbsp;<span>Pickup</span>
                <input type="radio" name="delivery_type" value="0">&nbsp;<span>Delivery<span>
                <br>
                <input type="hidden"  name="order_action" value="unpaid" readonly>
                
                
                <label>Time</label><br>
                <input type="text" name="delivery_time" id="delivery" placeholder="yy/mm/dd">
                </div>
                
                <div class="buttons">
                <input type="submit" name="submit" value="Place Order">
                <input type="reset" name="reset" value="Clear form">
                </div>

            </form>
        </div>
    </div>
</section>
@include('bakery.footer')
<script src="{{asset('bakery/app.js')}}"></script>
</body>
</html>
