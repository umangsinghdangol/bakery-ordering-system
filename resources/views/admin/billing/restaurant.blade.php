@extends('layouts.backend')
@section('title','List Order')
@section('content')
@section('css')
@endsection
@section('js')
@endsection
<section class="content-header">
    <h1>
        Restaurant Billing List
    </h1>
</section>
<section class="content">
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">List Order
            </h3>
        </div>

        @if(session()->has('success'))
            <div class="alert alert-success">
                {{session()->get('success')}}
            </div>
        @endif

        @if(session()->has('error'))
            <div class="alert alert-danger">
                {{session()->get('error')}}
            </div>
        @endif

        <div class="box-body">
            <table class="table table-bordered" id="category_table">
                <thead>
                <tr>
                    <th>SN</th>
                    <th>Name/Table No:</th>
                    <th>Address</th>
                    <th>Product Name</th>
                    <th>Product Qty</th>
                    <th>Rate</th>
                    <th>Total Amount</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                    @php($i=1)
                    @forelse ($orders as $order)
                        <tr>
                            <td>{{$i++}}</td>
                            <td>{{$order->name}}</td>
                            <td>{{$order->delivery_address}}</td>
                            <td>
                                <table>
                                    @foreach($order->product as $product)
                                        <tr>
                                            <td>{{$product->name}}</td>
                                        </tr>
                                    @endforeach
                                </table>
                            </td>
                            <td>
                                <table>
                                    @foreach($order->product as $product)
                                        <tr>
                                            <td>{{$product->pivot->qty}}</td>
                                        </tr>
                                    @endforeach
                                </table>
                            </td>

                            <td>
                                <table>
                                    @foreach($order->product as $product)
                                        <tr>
                                            <td>{{$product->price}}</td>
                                        </tr>
                                    @endforeach
                                </table>
                            </td>

                            <td>
                                {{$order->product()->sum('totalPrice')}}
                            </td>
                            <td>
                                <form action="{{route('admin.bakery.bill')}}" method="get">
                                    @csrf
                                    <input type="hidden" value="{{$order->id}}" id="bill_order_status" name="bill_order_status">
                                    <button type="submit">
                                        Bill
                                    </button>
                                </form>
                                <form action="{{route('admin.bakery.paid')}}" method="get">
                                    @csrf
                                    <input type="hidden" value="{{$order->id}}" id="bill_order_status" name="bill_order_status">
                                    <button type="submit">
                                        Paid
                                    </button>
                                </form>
                            </td>
                        </tr>
                    @empty
                        <p>There are no orders!!!!</p>
                        
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>
        
</section>
@endsection
