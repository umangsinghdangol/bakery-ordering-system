@extends('layouts.backend')
@section('title','List Order')
@section('content')
@section('css')
@endsection
@section('js')
@endsection
<section class="content-header">
    <h1>
        Restaurant Billing System
    </h1>
</section>
<section class="content">
    
    <div class="invoice-card">
        
        @forelse ($orders as $order)
        <a href="{{route('admin.billing.print',$order->id)}}">Print</a>
            <div class="invoice-title">
                <div id="main-title">
                    <h4>{{config('app.name')}}</h4>
                    <h4>Bharatpur, Chitwan, NEPAL</h4>
                </div>
                <span id="date">DATE : {{ date('m/d/Y H:i:s') }}</span>
                <span id="customer-name">customer name :&nbsp; {{$order->name}}</span>
                <span id="customer-address">Address : {{$order->delivery_address}}</span>
            </div>
            <div class="invoice-details">
            <table class="invoice-table">
                <thead>
                <tr>
                    <td>PRODUCT</td>
                    <td>Rate</td>
                    <td>UNIT</td>
                    <td>PRICE</td>
                </tr>
                </thead>
                
                <tbody>
                <tr class="row-data">
                    <td>
                        <table>
                            @foreach($order->product as $product)
                                <tr>
                                    <td>{{$product->name}}</td>
                                </tr>
                            @endforeach
                        </table>
                    </td>
                    <td>
                        <table>
                            @foreach($order->product as $product)
                                <tr>
                                    <td>{{$product->price}}</td>
                                </tr>
                            @endforeach
                        </table>
                    </td>
                    <td>
                        <table>
                            @foreach($order->product as $product)
                                <tr>
                                    <td>{{$product->pivot->qty}}</td>
                                </tr>
                            @endforeach
                        </table>
                    </td>
                    <td>
                        <table>
                            @foreach($order->product as $product)
                                <tr>
                                    <td>{{$product->pivot->qty * $product->price}}</td>
                                </tr>
                            @endforeach
                        </table>
                    </td>
                </tr>
                <tr class="calc-row">
                    <td colspan="3">Total</td>
                    <td>
                        {{$order->product()->sum('totalPrice')}}
                    </td>
                </tr>
                </tbody>
            </table>
            </div>
            
            <div class="invoice-footer">
                <span id="footer-details">Thanks for visiting {{config('app.name')}}.</span>
                <span id="signature">Signature</span>
            </div>
        </div>    
        @empty
            <p>There are no data!!!!</p>
        @endforelse
        
    
</section>
@endsection
