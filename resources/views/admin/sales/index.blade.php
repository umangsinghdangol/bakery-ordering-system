@extends('layouts.backend')
@section('title','List Order')
@section('content')
@section('css')
@endsection
@section('js')
@endsection
<section class="content-header">
    <h1>
        Sales Details
    </h1>
</section>
<section class="content">
    <form action="{{route('admin.sales.data')}}" method="get">
        <div class="input-group input-daterange">
            <input type="date" class="form-control" value="{{ date('m/d/Y H:i:s') }}" name="source_date">
            <div class="input-group-addon">to</div>
            <input type="date" class="form-control" value="{{ date('m/d/Y H:i:s') }}" name="destination_date">
        </div>
        <button type="submit" class="btn btn-info">
            Search
        </button>
    </form>   
    <div class="box-body">
        <table class="table table-bordered" id="category_table">
            <thead>
            <tr>
                <th>SN</th>
                <th>Name</th>
                <th>Email</th>
                <th>Phone Number</th>
                <th>Product Name</th>
                <th>Product Qty</th>
                <th>Rate</th>
                <th>Total</th>
            </tr>
            </thead>
            <tbody>
            @php($i=1)
            @forelse($orders as $order)
                    <tr>
                        <td>{{$i++}}</td>
                        <td>{{$order->name}}</td>
                        <td>{{$order->email}}</td>
                        <td>{{$order->phone_number}}</td>
                        <td>
                            <table>
                                @foreach($order->product as $product)
                                    <tr>
                                        <td>{{$product->name}}</td>
                                    </tr>
                                @endforeach
                            </table>
                        </td>

                        <td>
                            <table>
                                @foreach($order->product as $product)
                                    <tr>
                                        <td>{{$product->pivot->qty}}</td>
                                    </tr>
                                @endforeach
                            </table>
                        </td>

                        <td>
                            <table>
                                @foreach($order->product as $product)
                                    <tr>
                                        <td>{{$product->price}}</td>
                                    </tr>
                                @endforeach
                            </table>
                        </td>

                        <td>{{$order->product()->sum('totalPrice')}}</td>
                    </tr>

            @empty
                <p class="blank_data">There are no list of sales!!</p>
            @endforelse
            </tbody>
        </table>

</section>

@endsection
