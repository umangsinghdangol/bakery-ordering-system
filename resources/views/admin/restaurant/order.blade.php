@extends('layouts.backend')
@section('title','List Order')
@section('content')
@section('css')
@endsection
@section('js')
@endsection
<section class="content-header">
    <h1>
        Restaurant Food Order
    </h1>
</section>
<section class="content">
    <div class="container">
        <div class="table-add"></div>
        <div class="food-order">
            <div class="row">
                <div class="header">
                    <span>S.N</span>
                    <span>Food Name</span>
                    <span>Quantity</span>
                </div>
                <div class="body">
                    <div class="row">
                        <span id="food-1"></span>
                        <select name="food-menu" id="food-menu">
                            <option value="name of food">Name of food</option>
                        </select>
                        <input type="number" minimun=1 name="quantity">
                    </div>
                    
                </div>

            </div>
            
        </div>
    </div>
</section>
@endsection
