@extends('layouts.backend')
@section('title','List Order')
@section('content')
@section('css')
@endsection
@section('js')
@endsection
<section class="content-header">
    <h1>
        Admin Gallery List
    </h1>
</section>

<div class="container-fluid tm-container-content tm-mt-60 galleries_container">
    <div class="row tm-mb-90 tm-gallery">
        
        {{-- Image --}} 
        {{-- {{dd($galleries)}} --}}
        @forelse ($galleries as $gallery)
        <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-12 mb-5 galleries">
            <figure class="effect-ming tm-video-item figure_galleries">
                {{-- {{dd($gallery->gallery_image)}} --}}
                <a href="{{asset('image/gallery/'.$gallery->gallery_image)}}" target="_blank">
                    <img src="{{asset('image/gallery/'.$gallery->gallery_image)}}" alt="Image" class="img-fluid">
                    <input type="hidden" name="gallery_id" value="{{$gallery->id}}"/>
                    
                </a>
            </figure>
            <form action="{{route('admin.gallery.delete',$gallery->id)}}" method="post" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="gallery_image_name" value="{{$gallery->gallery_image}}" />
                <input type="submit" name="save" value="Delete" class="btn btn-danger dangers"/>
            </form>
        </div>
        @empty
            <p class="blank_data">There are no Image in Gallery</p>
        @endforelse
    </div> 
</div>

@endsection
