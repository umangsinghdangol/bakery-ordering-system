@extends('layouts.backend')
@section('title','List Order')
@section('content')
@section('css')
@endsection
@section('js')
@endsection
<section class="content-header">
    <h1>
        Admin Gallery
    </h1>
</section>
@if($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach($errors->all() as $error)
                <li>{{$error}}</li>
            @endforeach
        </ul>
    </div>
@endif
<form action="{{route('admin.gallery.store')}}" method="post" enctype="multipart/form-data">
    @csrf
    <div class="forms-group">
        <label for="image">Image</label>
        <input type="file" name="gallery_image">
    </div>
    <div class="form-group">
        <input type="submit" name="save" value="Save" class="btn btn-success"/>
    </div>
</form>

@endsection
