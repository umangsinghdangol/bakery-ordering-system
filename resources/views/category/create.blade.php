@extends('layouts.backend')
@section('title','Create Category')
@section('content')
    <section class="content-header">
        <h1>
            Category Management
        </h1>
    </section>
    <section class="content">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Create Category
                    <a href="{{route('category.index')}}"class="btn btn-success"><i class="fa fa-list"></i>List</a>
                </h3>
            </div>
            <div class="box-body">
                @if($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{$error}}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form action="{{route('category.store')}}" method="post">
                    @csrf
                    <div class="forms-group">
                        <label for="type">Category Type</label>
                        <select name="type" class="form-control">
                            <option>Select Category</option>
                            <option value="Bakery">Bakery</option>
                            <option value="Restaurant">Restaurant</option>
                        </select>
                    </div>
                    <div class="forms-group">
                        <label for="name">Name</label>
                        <input type="text" name="name" class="form-control" placeholder="Enter the name of Category">
                    </div>
                    <div class="form-group">
                        <label for="active">Status</label>
                        <input type="radio" id="active" name="status"  value="1">Active
                        <input type="radio" id="deactive" name="status" value="0" checked>De Active
                    </div>

                    <div class="form-group">
                        <input type="submit" name="save" value="Save Category" class="btn btn-success">
                        <input type="reset" name="reset" value="Clear" class="btn btn-danger">
                    </div>

                </form>

            </div>
        </div>
    </section>
@endsection
