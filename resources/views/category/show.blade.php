@extends('layouts.backend')
@section('title','View Category')
@section('content')
    <section class="content-header">
        <h1>
            Category Management
        </h1>
    </section>
    <section class="content">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Show Category
                    <a href="{{route('category.create')}}"class="btn btn-info"><i class="fa fa-plus"></i>create</a>
                    <a href="{{route('category.index')}}"class="btn btn-success"><i class="fa fa-list"></i>List</a>
                </h3>
            </div>
            <div class="box-body">
                <table class="table table-bordered">
                    <tr>
                        <th>Name</th>
                        <td>{{$category->name}}</td>
                    </tr>

                    <tr>
                        <th>Status</th>
                        <td>@if($category->status===1)
                                <label class="label label-success">Active</label>
                            @else
                                <label class="label label-danger">De Active</label>
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <th>Created At</th>
                        <td>{{$category->created_at}}</td>
                    </tr>
                    <tr>
                        <th>Updated At</th>
                        <td>{{$category->updated_at}}</td>
                    </tr>
                </table>

            </div>
        </div>
    </section>
@endsection
