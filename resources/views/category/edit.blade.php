@extends('layouts.backend')
@section('title','Create Category')
@section('content')
    <section class="content-header">
        <h1>
            Category Management
        </h1>
    </section>
    <section class="content">

        <!-- Default box -->
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Edit Category
                    <a href="{{route('category.index')}}"class="btn btn-success"><i class="fa fa-list"></i>List</a>
                </h3>
            </div>
            <div class="box-body">
                @if($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{$error}}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form action="{{route('category.update',$category->id)}}" method="post">
                    @csrf
                    @method('PUT')
                    <div class="forms-group">
                        <label for="name">Name</label>
                        <input type="text" name="name" class="form-control" value="{{$category->name}}">
                    </div>
                    <div class="form-group">
                        <label for="active">Status</label>
                        @if($category->status == 1)
                        <input type="radio" id="active" name="status"  value="{{$category->status}}" checked>Active
                        <input type="radio" id="deactive" name="status" value="0">De Active
                        @else
                            <input type="radio" id="active" name="status"  value="1">Active
                            <input type="radio" id="deactive" name="status" value="{{$category->status}}" checked>De Active
                            @endif
                    </div>

                    <div class="form-group">
                        <input type="submit" name="save" value="Update Category" class="btn btn-success">
                        <input type="reset" name="reset" value="Clear" class="btn btn-danger">
                    </div>

                </form>

            </div>
        </div>
    </section>
@endsection
