@extends('layouts.backend')
@section('title','List Category')
@section('content')
@section('css')
@endsection
@section('js')
@endsection
<section class="content-header">
    <h1>
        Category Management
    </h1>
</section>
<section class="content">
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">List Category
                <a href="{{route('category.create')}}"class="btn btn-info"><i class="fa fa-plus"></i>create</a>
            </h3>
        </div>

        @if(session()->has('success'))
            <div class="alert alert-success">
                {{session()->get('success')}}
            </div>
        @endif

        @if(session()->has('error'))
            <div class="alert alert-danger">
                {{session()->get('error')}}
            </div>
        @endif

        <div class="box-body">
            <table class="table table-bordered text-center"id="category_table">
                <thead>
                <tr>
                    <th>SN</th>
                    <th>Name</th>
                    <th>Type</th>
                    <th>Status</th>
                    <th>Action</th>
                    <th>Delete</th>
                </tr>
                </thead>
                <tbody>
                @php($i=1)
                @foreach($categories as $category)
                    <tr>
                        <td>{{$i++}}</td>
                        <td>{{$category->name}}</td>
                        <td>{{$category->type}}</td>
                        <td>
                            @if($category->status===1)
                                <label class="label label-success">Active</label>
                            @else
                                <label class="label label-danger">De Active</label>
                            @endif
                        </td>
                        <td>
                            <a href="{{route('category.edit',$category->id)}}" class="btn btn-warning"><i class="fa fa-pencil"></i></a>
                            <a href="{{route('category.show',$category->id)}}" class="btn btn-info"><i class="fa fa-eye"></i></a>
                        </td>
                        <td>

                            <form action="{{route('category.destroy',$category->id)}}" method="post" onsubmit="return confirm('Are you sure to delete')">
                                @csrf
                                <input type="hidden" name="_method" value="DELETE"/>
                                <button class="btn btn-danger"> <i class="fa fa-trash"></i></button>
                            </form>

                        </td>
                    </tr>
                @endforeach
                </tbody>
                <tfoot>
                </tfoot>
            </table>
        </div>
    </div>

</section>
@endsection
